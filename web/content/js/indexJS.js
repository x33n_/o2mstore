/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function servletData(category)
{
    $.ajax({
        url: "servleto2mStore?category=" + category,
        type: 'GET',
        success: function(data) {
            //parserData(urlFS);
            $("div.categories").html(data);
        },
        error: function() {
            alert('failure');
            $("div.padd p").html("Error!!!");
        }
    });
}

function buscarProd()
{
    clave1 = document.getElementById('busqueda').value;
    
    $.ajax({
        url: "busquedaServlet?busqueda="+clave1,
        type: 'get',
        success: function(data) {
            //parserData(urlFS);
            $("div.categories").html(data);
        }
    });
}

 function cargarAU(){
            $(".most-valued").hide();
            $(".box-body.recent-posts").hide();           
            $.ajax({   
                type: "GET",
                url:"content/html/aboutUs.jsp",
                data:{},
                success: function(data){       
                    $('div.categories').html(data);
                }
            });
}

 function cargarCU(){
            $(".most-valued").hide();
            $(".box-body.recent-posts").hide();
           
            $.ajax({   
                type: "GET",
                url:"content/html/contactUs.jsp",
                data:{},
                success: function(data){       
                    $('div.categories').html(data);
                }
            });
}

function cargarProfile(){
            $(".most-valued").hide();
            $(".box-body.recent-posts").hide();
            $.ajax({   
                type: "GET",
                url: "/o2mstore/loginServlet",
                data:{},
                success: function(data){       
                    $('div.categories').html(data);
                }
            });
}

function cargarReg(){
            $(".most-valued").hide();
            $(".box-body.recent-posts").hide();
            $.ajax({   
                type: "GET",
                url: "/o2mstore/content/html/register.jsp",
                data:{},
                success: function(data){       
                    $('div.categories').html(data);
                }
            });
}

function cargarLogin(loginErr){
            $(".most-valued").hide();
            $(".box-body.recent-posts").hide();
            
            $.ajax({   
                type: "POST",
                url: "/o2mstore/content/html/login.jsp",
                data:{},
                success: function(data){
                    $('div.errMsg').html(loginErr);
                    $('div.categories').html(data);
                }
            });
}

function cargarProduct(){
            $(".most-valued").hide();
            $(".box-body.recent-posts").hide();
            $.ajax({   
                type: "GET",
                url: "/o2mstore/content/html/newproduct.jsp",
                data:{},
                success: function(data){       
                    $('div.categories').html(data);
                }
            });
}



function mostrarMostValued(){
           
            $.ajax({   
                type: "GET",
                url:"MVServlet",
                data:{},
                success: function(data){       
                    $('div.most-valued').html(data);
                }
            });
}

function mostrarRecentPost(){
           
            $.ajax({   
                type: "GET",
                url:"RPServlet",
                data:{},
                success: function(data){       
                    $('div.box-body.recent-posts').html(data);
                }
            });
}

function comprobarContraseñas(){
    clave1 = document.f1.password.value;
    clave2 = document.f1.password2.value; 
    
    if (clave1 != clave2)
        {
            alert("Las dos claves son distintas...\nVuelve a introducir los datos") ;
            return false;
        }
        else{
            return true;
        }
        
}

/*function submitForm(){ 
    var validator = $("#f1").validate({
            rules: {
                    name: {
                            required: true,
                            maxlength: 50,
                            minlength: 5
                    },
                    apellidos: {
                            required: true,
                            maxlength: 50,
                            minlength: 5
                    },
                    password: {
                            required: false,
                            minlength: 6,
                            maxlength: 9
                    },
                    password2: {
                            required: true,
                            maxlength: 1000,
                            minlength: 10
                    },
                    email: {
                            required: true,
                            email: true,
                            maxlength: 1000,
                            minlength: 10
                    }
            },
            messages: {
                    'name': 'Debe ingresar el nombre',
                    'apellidos': 'Debe ingresar los apellido',
                    'password': 'Debe ingresar la contraseña',
                    'password2': 'Debe ingresar la confirmacion de la contraseña',
                    'email': { required: 'Debe ingresar un correo electrónico', email: 'Debe ingresar el correo electrónico con el formato correcto. Por ejemplo: u@localhost.com' }
            }
    });
    if(validator.form()){ // validation perform
		$('form#f1').attr({action: '/o2mstore/registerUser'});			
		$('form#f1').submit();
	}
}*/

function editProduct(nameProd)
{
    $(".most-valued").hide();
    $(".box-body.recent-posts").hide();
    $.ajax({
        url: "editProduct?nameprod=" + nameProd,
        type: 'GET',
        success: function(data) {
            //parserData(urlFS);
            $("div #editProduct").html(data);
        },
        error: function() {
            alert('failure');
            $("div.padd p").html("Error!!!");
        }
    });
}

function editProductPost()
{
    $.ajax({
        url: "",
        type: 'POST',
        success: function(data) {
            //parserData(urlFS);
            $("div #editProduct").html('');
        },
        error: function() {
            alert('failure');
            $("div.padd p").html("Error!!!");
        }
    });
}

function deleteProduct(idProduct)
{
    $.ajax({
        url: "deleteProduct?idProduct=" + idProduct,
        type: 'GET',
        success: function(data) {
            //parserData(urlFS);
            $("div.categories").html(data);
        },
        error: function() {
            alert('failure');
            $("div.padd p").html("Error!!!");
        }
    });
}

function closeProduct()
{
    $("div #editProduct").html('');
}

function cargarCarrito(){
    $(".most-valued").hide();
    $(".box-body.recent-posts").hide();
            $.ajax({   
                type: "POST",
                url: "pedidosServlet",
                data:{},
                success: function(data){       
                    $('div.categories').html(data);
                }
            });
}

function buyOneProduct(idProduct)
{
    $.ajax({
        url: "buyProduct?idproduct="+ idProduct,
        type: 'POST',
        success: function() {
            alert('Producto añadido al carro.');
        },
        error: (function () {
            alert('Error al procesar el pedido.');
        })
    });
}

function finalizarPedido()
{
    $.ajax({
        url: "pedidosServlet",
        type: 'Get',
        success: function () {
            alert('Pedido realizado.');
        },
        error: (function () {
            alert('Error al realizar el pedido.');
        })
    });
}