<%-- 
    Document   : newproduct
    Created on : 16-dic-2013, 10:52:02
    Author     : masterinftel21
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <body>

        <!-- Main content starts -->

        <div class="content">

            <div class="container-fluid">
                <div class="row-fluid">

                    <div class="span12">  

                        <div class="well login-register">

                            <h5>Registrar producto</h5>
                            <hr />

                            <div class="form">
                                <!-- Register product form -->
                                <form action="/o2mstore/productServlet" enctype='multipart/form-data' method="post" class="form-horizontal">
                                    <!-- Name Producto-->
                                    <div class="control-group">
                                        <label class="control-label" for="name">Nombre Producto</label>
                                        <div class="controls">
                                            <input name="name" type="text" class="input-large" id="name" maxlength="30" required>
                                        </div>
                                    </div>
                                    <!-- Categoria -->
                                    <div class="control-group">
                                        <label class="control-label" for="select">Categoria</label>
                                        <div class="controls">                               
                                            <select name="subcategoria" required>
                                                <option>&nbsp;</option>
                                                <option value="appUnEspecified">Aplicaciones</option>
                                                <option value="android">--Android</option>
                                                <option value="blackberry">--Blackberry</option>
                                                <option value="iphone">--iPhone</option>
                                                <option value="pelUnEspecified">Película</option>
                                                <option value="terror">--Terror</option>
                                                <option value="comedia">--Comedia</option>
                                                <option value="accion">--Accion</option>
                                                <option value="cienciaficcion">--Ciencia Ficción</option>
                                                <option value="animacion">--Animación</option>
                                                <option value="drama">--Drama</option>
                                                <option value="thriller">--Thriller</option>
                                                <option value="musUnEspecified">Música</option>
                                                <option value="pop">--Pop</option>
                                                <option value="rock">--Rock</option>
                                                <option value="metal">--Metal</option>
                                                <option value="clasica">--Clásica</option>
                                                <option value="hiphop">--Hip Hop</option>
                                                <option value="disco">--Disco</option>
                                                <option value="libUnEspecified">Libros</option>
                                                <option value="ciencias">--Ciencias</option>
                                                <option value="cocina">--Cocina</option>
                                                <option value="historia">--Historia</option>
                                                <option value="informatica">--Informática</option>
                                                <option value="clasicos">--Clásicos</option>
                                                <option value="infantil">--Infantil</option>
                                            </select>  
                                        </div>
                                    </div>
                                    <!-- Descripción -->
                                    <div class="control-group">
                                        <label class="control-label" for="descripcion">Breve descripción</label>
                                        <div class="controls">
                                            <input name="descripcion" type="descripcion" class="input-large" id="password" maxlength="100" required>
                                        </div>
                                    </div> 

                                    <!--Select Image-->
                                    <div class="control-group">
                                        <div class="controls">
                                            <input type="file" name="imageProd"/>
                                        </div>
                                    </div> 

                                    <!-- Buttons -->
                                    <div class="form-actions">
                                        <!-- Buttons -->
                                        <input type="submit" name="registrar" value="Registrar producto" class="btn btn-danger">
                                        <button type="reset" class="btn">Reset</button>
                                    </div>


                                </form>
                            </div> 


                        </div>

                    </div>

                </div>
            </div>

        </div>

        <script src="../js/custom.js"></script> <!-- Main js file -->
    </body>
</html>