<%-- 
    Document   : profile
    Created on : 16-dic-2013, 10:50:46
    Author     : masterinftel21
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <script src="content/js/indexJS.js"></script>
        <script src="content/js/jquery.js"></script>
        <script src="content/js/jquery.validate.js"></script>
        <script src="http://jquery.bassistance.de/validate/lib/jquery-1.9.0.js"></script>
</head>   
    <body>

        <!-- Main content starts -->

        <div class="content">

            <div class="matter">
                <div class="container-fluid">

                    <!-- Title starts -->
                    <div class="page-title">
                        <h2>Perfil</h2>
                        <hr />
                    </div>
                    <!-- Title ends -->

                    <!-- Breadcrumb starts -->

                    <ul class="breadcrumb">
                        <li><a href="#">Inicio</a> <span class="divider">/</span></li>
                        <li><a href="#">Perfil</a> <span class="divider">/</span></li>
                        <li class="active">Datos</li>
                    </ul>        

                    <!-- Breadcrumb ends -->

                    <hr />

                    <!-- Content starts -->

                    <div class="box-body">
                        <div class="row-fluid">

                            <div class="span4">

                                <div class="well">
                                    <!-- Your details -->
                                    <div class="address">
                                        <address>
                                            <h6>${user.nombre}</h6>
                                            ${user.apellidos}<br>
                                            ${user.rol}<br>
                                            <a href="mailto:#">${user.email}</a>
                                            <img src="data:image/gif;base64,${user.imagen}" width="120" height="120" alt="imagen"/>
                                        </address>
                                    </div>
                                </div>

                            </div>

                            <div class="span8">
                                <h5>Editar Perfil</h5>
                                <hr />
                                <div class="form">
                                    <!-- Edit profile form (not working)-->
                                    <!-- Register form (not working)-->
                                    <form action="/o2mstore/editProfile" onsubmit="return comprobarContraseñas()" enctype='multipart/form-data' method="post" class="form-horizontal">
                                        <!-- Name -->
                                        <div class="control-group">
                                            <label class="control-label" for="name">Nombre</label>
                                            <div class="controls">
                                                <input type="text" name="name" class="input-large" id="name"  maxlength="30" required>
                                            </div>
                                        </div>
                                        <!-- Apellidos -->
                                        <div class="control-group">
                                            <label  class="control-label" for="apellidos">Apellidos</label>
                                            <div class="controls">
                                                <input type="text" name="apellidos" class="input-large" id="apellidos" maxlength="50" required>
                                            </div>
                                        </div>
                                        <!-- Contraseña -->
                                        <div class="control-group">
                                            <label  class="control-label" for="password">Contraseña</label>
                                            <div class="controls">
                                                <input type="password" name="password" class="input-large" id="password" maxlength="20" required>
                                            </div>
                                        </div>
                                        <!-- Contraseña2 -->
                                        <div class="control-group">
                                            <label  class="control-label" for="password2">Repita la contraseña</label>
                                            <div class="controls">
                                                <input type="password" name="password2" class="input-large" id="password2" maxlength="20" required>
                                            </div>
                                        </div>
                                        <!-- Email -->
                                        <div class="control-group">
                                            <label  class="control-label" for="email">Email</label>
                                            <div class="controls">
                                                <input type="email" name="email" class="input-large" id="email" maxlength="40" required>
                                            </div>
                                        </div>                                   
                                        <!-- Fecha -->
                                        <div class="control-group">
                                            <label class="control-label" for="fechanac">Fecha de nacimiento</label>
                                            <div class="controls">
                                                <input type="date" name="date" class="input-large" id="fechanac">
                                            </div>
                                        </div>

                                        <!--Select Image-->
                                        <div class="control-group">
                                            <div class="controls">
                                                <input type="file" name="imageProf"/>
                                            </div>
                                        </div> 

                                        <!-- Buttons -->
                                        <div class="form-actions">
                                            <!-- Buttons -->
                                            <button type="submit" name="registrar" value="Registrate" class="btn btn-danger">Editar</button>
                                            <button type="reset" class="btn">Reset</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                                        
                            
                             
                            <div class="mini-title">
                                <h3>PRODUCTOS SUBIDOS</h3>
                            </div>
                            <div class="row-fluid">
                                <c:forEach var="prod" items="${productos}">
                                    <div class="span2">
                                        <!-- Image block -->
                                        <div class="box-body">
                                            <div class="block-image">
                                                <!-- Link -->
                                                <a>
                                                    <!-- Overlay content -->
                                                    <div class="image-overlay">
                                                        <div class="image-big-text">${prod.nombrep}</div>
                                                        <div class="image-small-text">${prod.descripcion}</div>
                                                        <br/>
                                                        
                                                        <button onclick="editProduct('${prod.nombrep}')">Editar</button>
                                                        <button onclick="deleteProduct(${prod.idproducto})">Eliminar</button>
                                                    </div>
                                                    <!-- Image link -->
                                                    <img src="data:image/gif;base64,${prod.imagen}" width="120" height="120" alt="test"/>

                                                </a>
                                            </div>
                                        </div>

                                    </div>
                                </c:forEach>
                            </div>       
                                        <div id="editProduct">            
                                            
                                        </div>
                        </div>
                    </div>

                    <!-- Content ends -->

                </div>
            </div>


        </div>
        <!-- Mainbar ends -->
        <div class="clearfix"></div>
   

    </div

    <div class="clearfix"></div>

    <!-- Main content ends -->


    <!-- Scroll to top -->
    <span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 

    <!-- JS -->
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.js"></script> <!-- Bootstrap -->
    <script src="../js/filter.js"></script> <!-- Filter JS -->
    <script src="../js/jquery.carouFredSel-6.1.0-packed.js"></script> <!-- CarouFredSel -->
    <script src="../js/jquery.flexslider-min.js"></script> <!-- Flexslider -->
    <script src="../js/jquery.isotope.js"></script> <!-- Isotope -->
    <script src="../js/jquery.prettyPhoto.js"></script> <!-- prettyPhoto -->
    <script src="../js/jquery.tweet.js"></script> <!-- Tweet -->
    <script src="../js/custom.js"></script> <!-- Main js file -->
</body>
</html>
