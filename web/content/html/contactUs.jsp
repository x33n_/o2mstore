<%-- 
    Document   : contactUs
    Created on : 15-dic-2013, 12:00:44
    Author     : David
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contáctanos - O2M Store</title>
    </head>
    <body>
        <div class="matter">
            <div class="container-fluid">

            <!-- Title starts -->
                <div class="page-title">
                    <h2>Contáctanos</h2>
                        <p>Ponte en contacto con el equipo de O2MStore</p>
                    <hr />
                </div>
                <!-- Title ends -->

                <!-- Breadcrumb starts -->

                <ul class="breadcrumb">
                    <li><a href="#">Inicio</a> <span class="divider">/</span></li>
                    <li class="active">Contáctanos</li>
                </ul>        

                <!-- Breadcrumb ends -->

                <hr />

                <!-- Content starts -->

                <div class="box-body contactus">

                    <div class="row-fluid">
                        <div class="span12">
                              <!-- Google maps -->
                              <div class="gmap">
                                 <!-- Google Maps. Replace the below iframe with your Google Maps embed code -->
                                 <iframe height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://mapsengine.google.com/map/embed?mid=zpjn--_ozCtY.k4q_vnDG_Qqo"></iframe>
                              </div>
                              
                        </div>
                    </div>
                        <div class="row-fluid">
                           <div class="span6">
                              <div class="well">
                                 <!-- Contact form -->
                                    <h5>Contáctanos</h5>
                                    <hr />
                                    <div class="form">
                                      <!-- Contact form (not working)-->
                                      <form class="form-horizontal">
                                          <!-- Name -->
                                          <div class="control-group">
                                            <label class="control-label" for="name">Nombre</label>
                                            <div class="controls">
                                              <input type="text" class="input-large" id="name">
                                            </div>
                                          </div>
                                          <!-- Email -->
                                          <div class="control-group">
                                            <label class="control-label" for="email">Email</label>
                                            <div class="controls">
                                              <input type="text" class="input-large" id="email">
                                            </div>
                                          </div>
                                          <!-- Website -->
                                          <div class="control-group">
                                            <label class="control-label" for="website">Página Web</label>
                                            <div class="controls">
                                              <input type="text" class="input-large" id="website">
                                            </div>
                                          </div>
                                          <!-- Comment -->
                                          <div class="control-group">
                                            <label class="control-label" for="comment">Comentario</label>
                                            <div class="controls">
                                              <textarea class="input-xlarge" id="comment" rows="3"></textarea>
                                            </div>
                                          </div>
                                          <!-- Buttons -->
                                          <div class="form-actions">
                                             <!-- Buttons -->
                                            <button type="submit" class="btn btn-danger">Enviar</button>
                                            <button type="reset" class="btn">Reset</button>
                                          </div>
                                      </form>
                                    </div>
                                 </div>
                           </div>
                           <div class="span6">
                                 <div class="well">
                                    <!-- Address section -->
                                       <h5>Dirección</h5>
                                       <hr />
                                       <div class="address">
                                           <address>
                                              <!-- Company name -->
                                              <h6>ETSIT Málaga</h6>
                                              <!-- Address -->
                                              Bulevar Louis Pasteur, 35<br>
                                              29071 Málaga<br>
                                              <!-- Phone number -->
                                              </address>
                                            
                                           
                                           
                                       </div>
                                 </div>
                           </div>
                        </div>

        </div>

        <!-- Content ends -->

      </div>
    </div>
        
        
        <!-- Scroll to top -->
<span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span>


<script src="../js/custom.js"></script> <!-- Main js file -->
<script src="../js/navigation.js"></script> <!-- Main js file -->
    </body>
</html>
