<%-- 
    Document   : aboutUs
    Created on : 15-dic-2013, 12:25:02
    Author     : David
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contáctanos - O2M Store</title>
    </head>
    <body>
        
        <div class="matter">
            <div class="container-fluid">

                <!-- Title starts -->
                <div class="page-title">
                  <h2>Sobre Nosotros</h2>
                  <p></p>
                  <hr />
                </div>
                <!-- Title ends -->

                <!-- Breadcrumb starts -->

                <ul class="breadcrumb">
                  <li><a href="#">Inicio</a> <span class="divider">/</span></li>
                  <li class="active">Sobre Nosotros</li>
                </ul>        

                <!-- Breadcrumb ends -->

                <hr />

                <!-- Content starts -->

                <div class="box-body aboutus">
                  <div class="row-fluid">
                                    <div class="span3">
                                      <!-- Staff #1 -->
                                      <div class="staff">
                                         <!-- Picture -->
                                         <div class="pic">
                                            <img src="/o2mstore/content/img/428233_3162231742878_1217858782_n.jpg" alt="" >
                                         </div>
                                         <!-- Details -->
                                         <div class="details">
                                            <!-- Name and designation -->
                                            <div class="desig pull-left">
                                               <p class="name">Juan Cañete Rodríguez</p>
                                               <em>Estudiante</em>
                                            </div>
                                            <!-- Social media details. Replace # with profile links -->
                                            <div class="asocial pull-right">
                                               <a href="#"><i class="icon-facebook"></i></a>
                                               <a href="#"><i class="icon-twitter"></i></a>
                                               <a href="#"><i class="icon-linkedin"></i></a>
                                            </div>
                                            <div class="clearfix"></div>
                                            <!-- Description -->
                                            <p class="adesc">Estudiante del Máster Inftel XI impartido por Fundación Vodafone y la UMA</p>
                                            <p><em>Lucena (Córdoba)</em><p>
                                         </div>
                                      </div>
                                   </div>
                                   <div class="span2">
                                      <!-- Staff #2 -->
                                      <div class="staff">
                                         <div class="pic">
                                            <img src="/o2mstore/content/img/descarga.gif" alt="" />
                                         </div>
                                         <div class="details">
                                            <div class="desig pull-left">
                                               <p class="name">Victor Caballero García</p>
                                               <em>Estudiante</em>
                                            </div>
                                            <div class="asocial pull-right">
                                               <a href="#"><i class="icon-facebook"></i></a>
                                               <a href="#"><i class="icon-twitter"></i></a>
                                               <a href="#"><i class="icon-linkedin"></i></a>
                                            </div>
                                            <div class="clearfix"></div>
                                            <p class="adesc">Estudiante del Máster Inftel XI impartido por Fundación Vodafone y la UMA</p>
                                            <p><em>Añora (Córdoba)</em></p>
                                         </div>
                                      </div>
                                   </div>
                                   <div class="span2">
                                      <!-- Staff #3 -->
                                      <div class="staff">
                                         <div class="pic">
                                            <img src="/o2mstore/content/img/599012_10200408849239512_1884685703_n.jpg" width="500" height="800" alt="" />
                                         </div>
                                         <div class="details">
                                            <div class="desig pull-left">
                                               <p class="name">David Cruz Toral</p>
                                               <em>Estudiante</em>
                                            </div>
                                            <div class="asocial pull-right">
                                               <a href="#"><i class="icon-facebook"></i></a>
                                               <a href="#"><i class="icon-twitter"></i></a>
                                               <a href="#"><i class="icon-linkedin"></i></a>
                                            </div>
                                            <div class="clearfix"></div>
                                            <p class="adesc">Estudiante del Máster Inftel XI impartido por Fundación Vodafone y la UMA</p>
                                            <p><em>Guadix (Granada)</em></p>
                                         </div>
                                      </div>
                                   </div>


                    </div>


          
                </div>

            <!-- Content ends -->

            </div>
        </div>
    
        <!-- Scroll to top -->
        <span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 

        <!-- JS -->
        <script src="content/js/custom.js"></script> <!-- Main js file -->
        
        
    </body>
</html>
