<%-- 
    Document   : mostvalued
    Created on : 16-dic-2013, 12:30:57
    Author     : David
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="mv" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
         <!-- Image blocks starts -->
              <div class="mini-title">
                <!-- Title -->
                <h3>Más Valorados</h3>
              </div>
             <div class="row-fluid">
                <mv:forEach var="prod" items="${product2}">
                    <div class="span4">
                      <!-- Image block -->
                      <div class="box-body">
                        <div class="block-image">
                          <!-- Link -->
                          <a href="#">
                            <!-- Overlay content -->
                            <div class="image-overlay">
                              <div class="image-big-text">${prod.nombrep}</div>
                              <div class="image-small-text">${prod.descripcion}</div>
                            </div>
                            <!-- Image link -->
                            <img src="data:image/gif;base64,${prod.imagen}"/>

                          </a>
                        </div>
                      </div>

                    </div>
                </mv:forEach>
              </div>

              <!-- Image blocks ends -->

              <hr />

 


        
    </body>
</html>
