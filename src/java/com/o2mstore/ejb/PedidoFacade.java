/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.o2mstore.ejb;

import com.o2mstore.jpa.Pedido;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Juan
 */
@Stateless
public class PedidoFacade extends AbstractFacade<Pedido> {
    @PersistenceContext(unitName = "O2MStorePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PedidoFacade() {
        super(Pedido.class);
    }
    
    public void saveProduct(Pedido p){
        em.persist(p);
    }
    
    public Integer getMaxID(){
        
        Integer id = (Integer) em.createQuery("select max(p.idpedido) "
                + "from Pedido p order by p.idpedido desc").getSingleResult();
        
        return id;
    }
    
    
    
}
