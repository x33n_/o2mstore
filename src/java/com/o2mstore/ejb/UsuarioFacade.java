/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.o2mstore.ejb;

import com.o2mstore.jpa.Usuario;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author masterinftel22
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {
    @PersistenceContext(unitName = "O2MStorePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
    public Usuario findByEmail(String email){
        Query query1 = em.createQuery("SELECT u FROM Usuario u WHERE u.email LIKE :value").setParameter("value", email);
        Usuario user;
        
        try{
            user = (Usuario) query1.getSingleResult();
        }
        catch(NoResultException e){
            user = null;
        }
        //return (Usuario) .getSingleResult();
        return user;
    }
    
    public Integer getMaxID(){
        
        Integer id = (Integer) em.createQuery("select max(u.idusuario) from Usuario u order by u.idusuario desc").getSingleResult();
        
        return id;
    }
    
    public Usuario findByNum(Integer number){
        Query query1 = em.createQuery("SELECT u FROM Usuario u WHERE u.idusuario = :value").setParameter("value", number);
        Usuario user;
        
        try{
            user = (Usuario) query1.getSingleResult();
        }
        catch(NoResultException e){
            user = null;
        }
        //return (Usuario) .getSingleResult();
        return user;
    }
    
    public Boolean tryLogin(String email, String password){
        
        Usuario user = findByEmail(email);
        
        System.out.println(user.getContrasena()+":"+user.getEmail());
        if(user.getContrasena().equals(password))
            return true;
        
        return false;
    }
    
    public void newUser(Usuario user){
        em.persist(user);
    }
}
