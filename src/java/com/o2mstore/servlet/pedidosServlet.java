/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.o2mstore.servlet;

import com.o2mstore.beans.ToolServlets;
import com.o2mstore.ejb.PedidoFacade;
import com.o2mstore.ejb.ProductoFacade;
import com.o2mstore.ejb.RealizaFacade;
import com.o2mstore.jpa.Pedido;
import com.o2mstore.jpa.Producto;
import com.o2mstore.jpa.Realiza;
import com.o2mstore.jpa.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author masterinftel21
 */
public class pedidosServlet extends HttpServlet {
    @EJB
    private PedidoFacade pedidoFacade;
    @EJB
    private RealizaFacade realizaFacade;
    @EJB
    private ProductoFacade productoFacade;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      /*  response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
             TODO output your page here. You may use following sample code. 
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet pedidosServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet pedidosServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }*/
        
         
          HttpSession miSesion= request.getSession(false);
         Usuario user = (Usuario) miSesion.getAttribute("user");
         
        // Integer idpedido = request.getParameter(pedidoFacade.idpedido);
        List<Producto> list2 = ToolServlets.getproductsFromMap(user);
         
         request.setAttribute("product2", list2);
        
        RequestDispatcher rd2;
        rd2 = request.getRequestDispatcher("/content/html/carrito.jsp");
        rd2.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Pedido pedido = new Pedido();
        pedido.setIdpedido(pedidoFacade.getMaxID()+1);
        pedido.setFecha(new Date());
        
        pedidoFacade.create(pedido);
        
         HttpSession miSesion= request.getSession(false);
         Usuario user = (Usuario) miSesion.getAttribute("user");
         
        // Integer idpedido = request.getParameter(pedidoFacade.idpedido);
        List<Producto> list2 = ToolServlets.getproductsFromMap(user);
        
        Realiza item = new Realiza();
        
        for(Producto p: list2)
        {
            item.setIdrealiza(realizaFacade.findMaxIdRealiza()+1);
            item.setIdusuario(user);
            item.setIdproducto(p);
            item.setIdpedido(pedido);
            
            realizaFacade.create(item);
        }
        
        ToolServlets.deleteAllProducts(user);
        
        RequestDispatcher rd2;
        rd2 = request.getRequestDispatcher("/indexStore.jsp");
        rd2.forward(request, response);
       
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
        
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
