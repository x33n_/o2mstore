/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.o2mstore.servlet;

import com.o2mstore.beans.ToolServlets;
import com.o2mstore.ejb.ProductoFacade;
import com.o2mstore.ejb.UsuarioFacade;
import com.o2mstore.jpa.Producto;
import com.o2mstore.jpa.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Juan
 */

@MultipartConfig
public class editProfile extends HttpServlet {
    @EJB
    private ProductoFacade productoFacade;
    @EJB
    private UsuarioFacade usuarioFacade;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet editProfile</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet editProfile at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
        
          
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        String email = request.getParameter("email");
        Usuario user =  usuarioFacade.findByEmail(email);
        
        String nombre = request.getParameter("name");
        String apellidos = request.getParameter("apellidos");
        String password = request.getParameter("password");
        String datosImg = ToolServlets.saveImageOnDataBase(request,"imageProf");

        user.setNombre(nombre);
        user.setApellidos(apellidos);
        user.setContrasena(password);
        user.setEmail(email);
        user.setFechanac(new Date(2011, 12, 20));
        if(user.getRol().equals("admin"))
            user.setRol("admin");
        else
            user.setRol("user");
        user.setImagen(datosImg);

//        System.out.println(user);
        usuarioFacade.edit(user);

//        HttpSession miSesion = request.getSession(true);
//        miSesion.setAttribute("user", user);
        //String emailSession = (String) miSesion.getAttribute("email");
        HttpSession miSesion= request.getSession(true);
        miSesion.setAttribute("user", user);
                
        response.sendRedirect("indexStore.jsp");
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
