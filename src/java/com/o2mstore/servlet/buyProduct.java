/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.o2mstore.servlet;

import com.o2mstore.beans.ToolServlets;
import com.o2mstore.ejb.PedidoFacade;
import com.o2mstore.ejb.ProductoFacade;
import com.o2mstore.ejb.RealizaFacade;
import com.o2mstore.jpa.Pedido;
import com.o2mstore.jpa.Producto;
import com.o2mstore.jpa.Realiza;
import com.o2mstore.jpa.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Juan
 */
public class buyProduct extends HttpServlet {
    @EJB
    private RealizaFacade realizaFacade;
    @EJB
    private PedidoFacade pedidoFacade;
    @EJB
    private ProductoFacade productoFacade;

    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet buyProduct</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet buyProduct at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        HttpSession miSesion= request.getSession(false);
//        Usuario user = new Usuario(999);
//        List<Producto> listProd = ToolServlets.getproductsFromMap(user);
//        
//        if (listProd != null) {
//            for (Producto prod : listProd) {
//                System.out.println("Producto: " + prod.getNombrep());
//            }
//        }
//        
//        Producto prod = new Producto(new Integer(1));
//        prod.setNombrep("HOLAAAA");
//        ToolServlets.setProductToMap(user, prod);
//        
//        
//        Producto prod2 = new Producto(new Integer(2));
//        prod2.setNombrep("HOLAAAA1");
//        ToolServlets.setProductToMap(user, prod2);
//        
//        Producto prod3 = new Producto(new Integer(3));
//        prod3.setNombrep("HOLAAAA2");
//        ToolServlets.setProductToMap(user, prod3);
//        
//        listProd = ToolServlets.getproductsFromMap(user);
//        System.out.println("Size: " + listProd.size());
//        
//        for(Producto prod11:listProd)
//        {
//            System.out.println("Producto: " + prod11.getNombrep());
//        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        HttpSession miSesion= request.getSession(false);
        Usuario user = (Usuario) miSesion.getAttribute("user");
        System.out.println("User: " + user.getEmail());
        String idProd = request.getParameter("idproduct");
        System.out.println("idProduct: "+ idProd);
        Integer idProduct = new Integer(idProd);
        
        Producto prod = productoFacade.find(idProduct);
        System.out.println("Producto: " + prod.getDescripcion());
        
        ToolServlets.setProductToMap(user, prod);
        /*Pedido pedido = pedidoFacade.find(new Integer(1));
        System.out.println("pedido: " + pedido.getIdpedido());
        Realiza realizaPed = new Realiza(user.getIdusuario());
        System.out.println("realizaPed: " + realizaPed.getIdusuario());
        
        realizaPed.setIdpedido(pedido);
        realizaPed.setIdproducto(prod);
        
        Integer idRealiza = realizaFacade.findMaxIdRealiza();
        idRealiza++;
        
        realizaPed.setIdrealiza(idRealiza);
        realizaPed.setIdusuario(user);
        
        realizaFacade.create(realizaPed);*/
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
