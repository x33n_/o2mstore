/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.o2mstore.servlet;
import com.o2mstore.ejb.ProductoFacade;
import com.o2mstore.jpa.Producto;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author masterinftel22
 */
public class servleto2mStore extends HttpServlet {
    
    @EJB
    private ProductoFacade productoFacade;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            // TODO output your page here. You may use following sample code. 
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet servleto2mStore</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet servleto2mStore at " + request.getParameter("categoria") + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }*/
        
        //Product prod = productFacade.find(new Integer(980025));
        String category = request.getParameter("category");
        List<Producto> list = productoFacade.findByCategory(category);
        
       
        
        
        //Blob imageReturn = productFacade.getImageProduct(new Integer(980001));
        
        
        
        // Set content type
        //resp.setContentType(/* set mime type of your image, might want to store that info in the DB too */);
        /*        InputStream in = null;
                OutputStream out = null;
                try {
                        response.setContentLength((int) imageReturn.length());
                        // Open the file and output streams
                        in = imageReturn.getBinaryStream();
                        out = response.getOutputStream();
                        // Copy the contents of the file to the output stream
                        byte[] buf = new byte[1024];
                        int count = 0;
                        while ((count = in.read(buf)) >= 0) {
                                out.write(buf, 0, count);
                        }
                        //long end = System.currentTimeMillis();
                } catch (SQLException e) {
                        // do something useful when reading image from DB fails
                } finally {
                
                }
        
        */
        
        String param = request.getParameter("category");
        request.setAttribute("category", param);
        request.setAttribute("product", list);
        //request.setAttribute("image", out);
        
        RequestDispatcher rd;
        
        rd = request.getRequestDispatcher("/content/html/categoriesProd.jsp");
        rd.forward(request, response);
        
       
        
       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
