/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.o2mstore.beans;

import com.o2mstore.jpa.Producto;
import com.o2mstore.jpa.Usuario;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import sun.misc.BASE64Encoder;

/**
 *
 * @author Juan
 */
public class ToolServlets {
    
    private static Map<Integer,List<Producto>> mapCurrentReq = new HashMap<Integer, List<Producto>>();
    
    public static String saveImageOnDataBase(HttpServletRequest request,String nameId) throws ServletException, IOException {
        // Es un formulario multipart, cogeremos cada una de esas partes
        String contentType = request.getContentType();

        if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) {
            // Cogemos el flujo de entrada
            DataInputStream in = new DataInputStream(request.getPart(nameId).getInputStream());

            // Empezamos a leer
            long longit = request.getPart(nameId).getSize();

            int longitud = (int) longit;

            byte dataBytes[] = new byte[longitud];
            int byteLeido = 0;
            int numBytesLeidos = 0;

            while (numBytesLeidos < longitud) {

                byteLeido = in.read(dataBytes, numBytesLeidos, longitud);
                numBytesLeidos += byteLeido;
            }

            // Codificamos a Base64
            BASE64Encoder e = new BASE64Encoder();
            String datos = e.encodeBuffer(dataBytes);

            return datos;
        }

        return null;
    }
    
    public static List<Producto> getproductsFromMap(Usuario user)
    {
        return mapCurrentReq.get(user.getIdusuario());
    }
    
    public static void setProductToMap(Usuario user,Producto product)
    {
        List <Producto> listProd = mapCurrentReq.get(user.getIdusuario());
        
        
        if(listProd == null)
        {
            listProd = new LinkedList<>();
        }
        
        listProd.add(product);
        
        mapCurrentReq.put(user.getIdusuario(), listProd);
    }
    
    public static void deleteAllProducts(Usuario user)
    {
        mapCurrentReq.remove(user.getIdusuario());
    }
}
